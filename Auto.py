#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from Estanque import Estanque
from Motor import Motor
from Rueda import Rueda
from Velocimetro import Velocimetro

import random

class Auto():
	def __init__(self, combustible, cilindrada, encendido, cantidad, velocidad, distancia):
		self.estanque = Estanque(combustible)
		self.motor = Motor(cilindrada, encendido)
		self.rueda = Rueda(cantidad)
		self.velocimetro = Velocimetro(velocidad, distancia)
	
