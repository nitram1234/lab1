#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random

class Velocimetro():
	def __init__(self, velocidad, distancia):
		self.velocidad = velocidad
		self.distancia = distancia
		
		
	def variacion_de_velocidad(self, velocidad, accion):
		
		if accion == "A":
			velocidad = (velocidad + 24)
			if velocidad >= 120:
				velocidad = 120			
		elif accion == "D":
			velocidad = (velocidad - 24)
			if velocidad <= 0:
				velocidad = 0
		
		return velocidad
	
	def variacion_de_distancia(self, distancia, velocidad):
		
		tiempo = 0.3
		distancia_recorrida = (velocidad * tiempo)
		distancia = (distancia + distancia_recorrida)
		
		return distancia
