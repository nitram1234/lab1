#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from Auto import Auto
from Estanque import Estanque
from Motor import Motor
from Rueda import Rueda
from Velocimetro import Velocimetro

import random

if __name__ == '__main__':
	
	combustible = 32
	random_dela_cilindrada = [1.2, 1.6]
	cilindrada = random_dela_cilindrada[random.randint(0, 1)]
	cantidad = 4	
	velocidad = 0
	distancia = 0
	encendido = 1
	
	auto = Auto(combustible, cilindrada, encendido, cantidad, velocidad, distancia)
		
	auto.motor.encendido = int(input("para encender el motor ingrese 0: "))
	
	combustible_f = auto.estanque.combustible
	accion_f = auto.motor.encendido
	velocidad_f = auto.velocimetro.velocidad
	distancia_f = auto.velocimetro.distancia
	cilindrada_f = auto.motor.cilindrada
	
	auto.estanque.combustible = auto.estanque.gasto(combustible_f, accion_f, velocidad_f, distancia_f, cilindrada_f)
	
	print("la velocidad actual es: {0} km/h".format(auto.velocimetro.velocidad))
	print("el combustible que le queda es: {0}".format(auto.estanque.combustible))	



	while auto.motor.encendido == 0:
		
		ciclos = 0
		
		print("Para acelerar ingrese ·A·")
		print("Para frenar ingrese ·D·")
		print("Para apagar el motor ingrese a")
		accion = input()
		
		if accion == "a":
			auto.motor.encendido = 1
		
		velocidad_f = auto.velocimetro.velocidad
		distancia_f = auto.velocimetro.distancia
#		tiempo_f = 
		
		auto.velocimetro.velocidad = auto.velocimetro.variacion_de_velocidad(velocidad_f, accion)
		print("su velocidad actual es: {0} km/h".format(auto.velocimetro.velocidad))
		auto.velocimetro.distancia = auto.velocimetro.variacion_de_distancia(distancia_f, auto.velocimetro.velocidad)
		print("la distancia recorrida es: {0} km".format(auto.velocimetro.distancia))
		
		combustible_f = auto.estanque.combustible
		accion_f = auto.motor.encendido
		velocidad_f = auto.velocimetro.velocidad
		distancia_f = auto.velocimetro.distancia
		cilindrada_f = auto.motor.cilindrada
		
		auto.estanque.combustible = auto.estanque.gasto(combustible_f, accion_f, velocidad_f, distancia_f, cilindrada_f)
		print("el combustible que le queda es: {0} litros".format(auto.estanque.combustible))
		
		if auto.estanque.combustible == 0:
			print("Se le acabo la bencina :c")
			print("la distancia que alcanzo a recorrer fue: {0} km".format(auto.velocimetro.distancia))
			while auto.velocimetro.velocidad > 0:
				auto.velocimetro.velocidad -= 10
				if auto.velocimetro.velocidad < 10:
					auto.velocimetro.velocidad = 0
				print("su velocidad es: {0} km/h".format(auto.velocimetro.velocidad))
			print("chau")
			auto.motor.encendido = 1
		
		
#		if accion == "A":
#			auto.velocimetro.velocidad += 5
#			
#			if auto.estanque.combustible < 2:
#				auto.estanque.combustible = 0
#			else:
#				auto.estanque.combustible -= 2
#				
#			print("La velocidad actual es: {0}km/h".format(auto.velocimetro.velocidad))
#			print("El combustible que le queda es: {0}litros".format(auto.estanque.combustible))
#			accion = "C"
#			
#			
#		elif accion == "C":
#			
#			if auto.estanque.combustible < 1:
#				auto.estanque.combustible = 0
#			else:
#				auto.estanque.combustible -= 1
#				
#			print("el auto se encuantra a velocidad constante: {0}km/h".format(auto.velocimetro.velocidad))
#			print("ingrese ·A· para acelerar o ·D· para desacelerar: ")
#			print("El combustible que le queda es: {0}litros".format(auto.estanque.combustible))
#			accion = input()
#			
#		elif accion == "D":
#			
#			if auto.velocimetro.velocidad < 6:
#				auto.velocimetro.velocidad = 0
#			else: 
#				auto.velocimetro.velocidad -=6
#			
#			if auto.estanque.combustible < 0.5:
#				auto.estanque.combustible = 0
#			else:
#				auto.estanque.combustible -= 0.5
#				
#			print("La velocidad actual es: {0}km/h".format(auto.velocimetro.velocidad))
#			print("El combustible que le queda es: {0}litros".format(auto.estanque.combustible))
#			accion = "C"
#		
#		if auto.estanque.combustible == 0:
#			print("a su auto no le queda combutible")
#			while auto.velocimetro.velocidad >= 0:
#				auto.velocimetro.velocidad -= 1
#				if auto.velocimetro.velocidad < 1:
#					auto.velocimetro.velocidad = 0
#				print("su velocidad actual es: {0}km/h".format(auto.velocimetro.velocidad))
#			auto.motor.encendido = 1
			
		ciclos = ciclos + 1


	
